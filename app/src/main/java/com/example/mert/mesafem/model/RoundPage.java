package com.example.mert.mesafem.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mert on 17/07/2017.
 */

public class RoundPage {

    private  Round round;

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }
}
