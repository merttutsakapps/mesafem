package com.example.mert.mesafem.anims;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.example.mert.mesafem.activities.BaseActivity;

public class OpenPageAnimator extends AnimatorListenerAdapter {

    private View content;
    private BaseActivity baseActivity;
    private boolean isStackRecord;
    private int x;
    private int y;
    private float finalRounds;

    public OpenPageAnimator(BaseActivity baseActivity, boolean isStackRecord, View content) {
        this.content = content;
        this.baseActivity = baseActivity;
        this.isStackRecord = isStackRecord;

        x = (content.getLeft() + content.getRight()) / 2;
        y = (content.getTop() + content.getBottom()) / 2;

        finalRounds = (float) Math.max(content.getWidth(), content.getHeight());

        content.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationStart(Animator animation) {
        animation = ViewAnimationUtils.createCircularReveal(content, x, y, finalRounds, 0);
        animation.setDuration(100);
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
        x = (content.getLeft() + content.getRight()) / 2;
        y = (content.getTop() + content.getBottom()) / 2;

        finalRounds = (float) Math.max(content.getWidth(), content.getHeight());

        content.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        super.onAnimationEnd(animation);

        content.setVisibility(View.GONE);
    }
}
