package com.example.mert.mesafem.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mert on 07/07/2017.
 */

public class Round extends RealmObject {
    @PrimaryKey
    private int id;
    private int roundId;
    private long time;
    private double kilometers;
    private long pace;
    private RealmList<Location> locationses;
    private Date createdAt;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int paceId) {
        this.roundId = paceId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getKilometers() {
        return kilometers;
    }

    public void setKilometers(double kilometers) {
        this.kilometers = kilometers;
    }

    public long getPace() {
        return pace;
    }

    public void setPace(long pace) {
        this.pace = pace;
    }

    public RealmList<Location> getLocationses() {
        return locationses;
    }

    public void setLocationses(RealmList<Location> locationses) {
        this.locationses = locationses;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Round{" +
                "id=" + id +
                ", roundId=" + roundId +
                ", time=" + time +
                ", kilometers=" + kilometers +
                ", pace=" + pace +
                ", locationses=" + locationses +
                ", createdAt=" + createdAt +
                '}';
    }
}
