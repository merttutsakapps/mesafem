package com.example.mert.mesafem.dialogs;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;

/**
 * Created by mert on 11/07/2017.
 */

public class CloseDialog extends Dialog {

    private BaseActivity baseActivity;

    public CloseDialog(@NonNull BaseActivity baseActivity) {
        super(baseActivity);
        this.baseActivity = baseActivity;
    }

    @Override
    public void create() {
        super.create();

        this.setContentView(R.layout.dialog_close);

        final Button button_positive = (Button) this.findViewById(R.id.buttonPositive);
        final Button button_negative = (Button) this.findViewById(R.id.buttonNegative);

        this.setCancelable(false);

        button_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseActivity.finish();
            }
        });

        button_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (!this.isShowing()) {
            this.show();
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

}
