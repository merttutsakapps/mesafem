package com.example.mert.mesafem.adapters;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.formats.TimeFormatted;
import com.example.mert.mesafem.model.Header;
import com.example.mert.mesafem.model.RoundPage;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by mert on 17/07/2017.
 */

public class RoundListAdapter extends BaseAdapter {

    private BaseActivity baseActivity;
    private List<RoundPage> roundPages;

    public RoundListAdapter(BaseActivity baseActivity, List<RoundPage> rounds) {
        super();
        this.baseActivity = baseActivity;
        this.roundPages = rounds;
    }

    @Override
    public int getCount() {
        return roundPages.size();
    }

    @Override
    public Object getItem(int i) {
        return roundPages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int itemPosition, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) baseActivity.getSystemService(baseActivity.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.listview_round_row, null);
        }

        Log.i("ROUND_LIST_ADAPTER", "roundPages(" + (itemPosition + 1) + ") : " + roundPages.get(itemPosition).getRound().toString());

        TextView kilometers_id = (TextView) view.findViewById(R.id.textView_kilometers_id);
        TextView pace = (TextView) view.findViewById(R.id.textView_pace);
        TextView distance = (TextView) view.findViewById(R.id.textView_distance);

        kilometers_id.setText((itemPosition + 1) + ")");

        String convertPace = new TimeFormatted().convertPace(roundPages.get(itemPosition).getRound().getPace());

        pace.setText(convertPace);

        if (itemPosition == 0) {
            distance.setTextColor(ContextCompat.getColor(baseActivity, R.color.colorApp));
            distance.setText("---");
        } else {
            long lastPace = roundPages.get(itemPosition).getRound().getPace();
            long beforePace = roundPages.get(itemPosition - 1).getRound().getPace();
            long distancePace = lastPace - beforePace;
            String distancePaceText = new TimeFormatted().convertPace(distancePace);

            int distanceMinute = Integer.parseInt(new TimeFormatted().getMinute(Math.abs(distancePace)));
            int distanceSecond = Integer.parseInt(new TimeFormatted().getSecond(Math.abs(distancePace)));
            distancePace = (distanceMinute * 3600) + (distanceSecond * 60);
            Log.i("DISTANCE_ROUND_PACE", "distance pace : " + distancePace);
            if (distancePace > 0) {
                distance.setTextColor(Color.rgb(0, 1616, 0));
                distance.setText("+" + distancePaceText);
            } else if (distancePace == 0) {
                distance.setTextColor(ContextCompat.getColor(baseActivity, R.color.colorApp));
                distance.setText("---");
            } else {
                distance.setTextColor(Color.rgb(1616, 0000, 0000));
                distance.setText("-" + distancePaceText);
            }
        }
        return view;
    }
}
