package com.example.mert.mesafem.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.permissions.RequestPermissions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import static android.widget.Toast.LENGTH_LONG;


public class GirisActivity extends AppCompatActivity {

    protected Location mLastLocation;

    private RequestPermissions requestPermissions;

    private FusedLocationProviderClient mFusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestPermissions = new RequestPermissions(GirisActivity.this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (!requestPermissions.checkPermissions()) {
            Log.d("PERMISSION", "İZİN ÇIKMADI");
            requestPermissions.requestPermissions();
        } else {

            Log.d("PERMISSION", "İZİN ÇIKTI");

            appOpen();
        }
    }

    private void appOpen() {
        setContentView(R.layout.activity_open);


        getLastLocation();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(GirisActivity.this, MainActivity.class);
                startActivity(intent);
                GirisActivity.this.finish();
            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("PERMISSION", "onRequestPermissionResult");
        if (requestCode == requestPermissions.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.d("PERMISSION", "User interaction was cancelled.");
            }
            if ((grantResults[0] == PackageManager.PERMISSION_GRANTED) && (grantResults[1] == PackageManager.PERMISSION_GRANTED) && (grantResults[2] == PackageManager.PERMISSION_GRANTED)) {
                Log.d("PERMISSION", " AKTİFLİK SAĞLANDI");
                appOpen();
            } else {
                Toast.makeText(this, "You can not continue without permission.", LENGTH_LONG).show();
                finish();
            }
        }
    }




    @SuppressWarnings("MissingPermission")
    public void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                        } else {
                            Log.d("LAST_LOCATION", getString(R.string.no_location_detected).toString());

                        }
                    }
                });
    }


}
