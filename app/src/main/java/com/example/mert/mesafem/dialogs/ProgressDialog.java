package com.example.mert.mesafem.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;


public class ProgressDialog extends Dialog {

    private BaseActivity baseActivity;

    public ProgressDialog(@NonNull BaseActivity baseActivity) {
        super(baseActivity);
        this.baseActivity = baseActivity;
    }

    public void onCreateDialog(@Nullable String metin) {
        this.setContentView(R.layout.dialog_progres);

        final TextView textView = (TextView) this.findViewById(R.id.textViewProgressMetin);
        final ProgressBar progressBar = (ProgressBar) this.findViewById(R.id.progressBar);

        progressBar.setActivated(true);

        if (metin == "" || metin == null) {
            progressBar.setBackgroundColor(Color.TRANSPARENT);
            textView.setVisibility(View.GONE);
        } else {
            progressBar.setBackgroundResource(R.color.colorApp);
            textView.setVisibility(View.VISIBLE);
            textView.setText(metin);
        }
        if (!this.isShowing()) {
            this.show();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }
}


