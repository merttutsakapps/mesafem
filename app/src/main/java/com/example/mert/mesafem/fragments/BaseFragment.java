package com.example.mert.mesafem.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.dialogs.ProgressDialog;
import com.example.mert.mesafem.preferences.SharedPreference;

public class BaseFragment extends Fragment {

    //ACTIVITIES
    protected BaseActivity baseActivity;
    //PREFERENCES
    protected SharedPreference sharedPreference;
    //DIALOGS
    protected ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseActivity = (BaseActivity) getActivity();
        sharedPreference = new SharedPreference(baseActivity);

        baseActivity.setTextHeadermid(getResources().getString(R.string.app_name));
        baseActivity.setVisibilityHeaderButton(false, true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide(Gravity.RIGHT);
            baseActivity.getWindow().setExitTransition(slide);
        }
        progressDialog = new ProgressDialog(baseActivity);
    }

    protected void setPageName(String name) {
        Bundle args = new Bundle();
        args.putString("pageName", name);
        Log.i("BUNDLE", "pageName : " + name);
        baseActivity.setArguments(args);
    }

    protected boolean replaceFragment(BaseFragment _fragment) {
        if (_fragment != null) {
            baseActivity.changeFragment(_fragment);
            return true;
        } else {
            return false;
        }
    }

    protected boolean setHeaderTextActionBar(String _text) {
        if (_text != null || _text != "") {
            baseActivity.setTextHeadermid(_text);
            return true;
        } else {
            return false;
        }
    }

    protected boolean setButtonVisibilityActionBar(boolean _left, boolean _right) {
        baseActivity.setVisibilityHeaderButton(_left, _right);
        return true;
    }

    protected boolean backPressButton() {
        baseActivity.onBackPressed();
        return true;
    }
}