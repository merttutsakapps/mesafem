package com.example.mert.mesafem.activities;

import android.os.Bundle;

import com.example.mert.mesafem.fragments.SignUpFragment;
import com.example.mert.mesafem.fragments.MainStartFragment;
import com.example.mert.mesafem.preferences.SharedPreference;

public class MainActivity extends BaseActivity   {


    private SharedPreference sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreference = new SharedPreference(this);
        if (sharedPreference.isEmpty()) {
            initView(new SignUpFragment());
        }else{
            initView(new MainStartFragment());
        }
    }
}
