package com.example.mert.mesafem.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.mert.mesafem.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mert on 28/06/2017.
 */

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> views;

    private String tabTitles[] = new String[]{"START", "HISTORY"};

    public SampleFragmentPagerAdapter(FragmentManager fm, ArrayList<BaseFragment> fragments) {
        super(fm);

        views = fragments;

        saveState();
    }


    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public Fragment getItem(int position) {
        return views.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }


}