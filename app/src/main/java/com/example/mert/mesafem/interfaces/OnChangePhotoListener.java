package com.example.mert.mesafem.interfaces;

import android.support.annotation.Nullable;

/**
 * Created by mert on 20/07/2017.
 */

public interface OnChangePhotoListener {
    void ChangePhoto(@Nullable String path);
}
