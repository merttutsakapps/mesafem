package com.example.mert.mesafem.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.example.mert.mesafem.bitmaps.BitmapModifyOrientation;
import com.example.mert.mesafem.R;
import com.example.mert.mesafem.dialogs.SelectionPhotoDialog;
import com.example.mert.mesafem.interfaces.OnChangePhotoListener;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilUpdateFragment extends BaseFragment implements OnChangePhotoListener {

    private SelectionPhotoDialog selectionPhotoDialog;

    private Button buttonNext_u;
    private CircleImageView imageViewProfilImage_u;
    private EditText editTextFirstName_u;
    private EditText editTextLastName_u;
    private EditText editTextEmail_u;
    private EditText editTextHeight_u;
    private EditText editTextWeight_u;

    private String[] editTextsText;
    private EditText[] editTexts;

    private String profilImagePath = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_profil_update, container, false);

        setPageName("ProfilUpdate");
        setHeaderTextActionBar(getResources().getString(R.string.settings));
        setButtonVisibilityActionBar(true, false);

        selectionPhotoDialog = new SelectionPhotoDialog(baseActivity);
        baseActivity.setChangePhotoListener(ProfilUpdateFragment.this);

        buttonNext_u = (Button) view.findViewById(R.id.button_save_u);
        editTextFirstName_u = (EditText) view.findViewById(R.id.editText_first_name_u);
        editTextLastName_u = (EditText) view.findViewById(R.id.editText_last_name_u);
        editTextEmail_u = (EditText) view.findViewById(R.id.editText_email_u);
        editTextHeight_u = (EditText) view.findViewById(R.id.editText_height_u);
        editTextWeight_u = (EditText) view.findViewById(R.id.editText_weight_u);

        imageViewProfilImage_u = (CircleImageView) view.findViewById(R.id.imageView_profil_image_u);

        Log.d("PROFIL_PHOTO", "START profilupdate");
        profilImagePath = setProfilImage();

        progressDialog.onCreateDialog("Your process is running. Please wait!");

        editTextFirstName_u.setHint(sharedPreference.getFirstName());
        editTextLastName_u.setHint(sharedPreference.getLastName());
        editTextEmail_u.setHint(sharedPreference.getEMail());
        editTextHeight_u.setHint(String.valueOf(sharedPreference.getHeight()));
        editTextWeight_u.setHint(String.valueOf(sharedPreference.getWeight()));
        progressDialog.dismiss();

        imageViewProfilImage_u.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("IMAGE_CLICK", "tıklandı.");
                selectionPhotoDialog.create();
            }
        });
        buttonNext_u.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ControlContent()) {
                    progressDialog.onCreateDialog("Your process is running. Please wait!");
                    backPressButton();
                    progressDialog.dismiss();
                }
            }
        });

        return view;
    }

    private boolean ControlContent() {

        editTextsText = new String[]{
                editTextFirstName_u.getText().toString(),
                editTextLastName_u.getText().toString(),
                editTextEmail_u.getText().toString(),
                editTextHeight_u.getText().toString(),
                editTextWeight_u.getText().toString()
        };

        for (int item = 0; item < editTextsText.length; item++) {
            Log.d("ITEM", String.valueOf(item));
            if (!editTextsText[item].isEmpty()) {

                if (item == 0) {
                    sharedPreference.setFirstName(editTextsText[item]);
                }
                if (item == 1) {
                    sharedPreference.setLastName(editTextsText[item]);
                }
                if (item == 2) {
                    if ((editTextsText[2].substring(0, 1) == "@")) {
                        Log.d("EMAIL_CONTROL", ".substring(0, 1) == @");
                        Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format or leave empty", Toast.LENGTH_LONG).show();
                        return false;
                    } else if ((editTextsText[2].indexOf(" ") != -1)) {
                        Log.d("EMAIL_CONTROL", ".indexOf(' ') != -1");
                        Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format or leave empty", Toast.LENGTH_LONG).show();
                        return false;
                    } else if ((editTextsText[2].indexOf("@") == -1)) {
                        Log.d("EMAIL_CONTROL", ".indexOf(@) == -1");
                        Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format or leave empty", Toast.LENGTH_LONG).show();
                        return false;
                    } else if ((editTextsText[2].indexOf(".com") == -1)) {
                        Log.d("EMAIL_CONTROL", ".indexOf(.com) == -1");
                        Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format or leave empty", Toast.LENGTH_LONG).show();
                        return false;
                    } else if ((editTextsText[2].indexOf("@.com") != -1)) {
                        Log.d("EMAIL_CONTROL", ".indexOf(@) == -1");
                        Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format.", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    sharedPreference.setEMail(editTextsText[2]);
                    //sharedPreference.setLastName(editTextsText[item]);
                    return true;
                }
                if (item == 3) {
                    if (editTextsText[item].indexOf('.') != -1) {
                        if (Integer.parseInt(editTextsText[item].substring(0, editTextsText[item].indexOf("."))) < 100) {
                            Log.d("HEIGHT_CONTROL", "Invalid height.");
                            Toast.makeText(baseActivity, "Your height can't be less than 100 meters.", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    } else {
                        if (Integer.parseInt(editTextsText[item]) < 100) {
                            Log.d("HEIGHT_CONTROL", "Invalid height.");
                            Toast.makeText(baseActivity, "Your height can't be less than 100 meters.", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }
                    sharedPreference.setHeight(Float.parseFloat(editTextsText[item]));
                    return true;
                }
                if (item == 4) {
                    if (editTextsText[item].indexOf('.') != -1) {
                        if (Integer.parseInt(editTextsText[item].substring(0, editTextsText[item].indexOf("."))) < 40) {
                            Log.d("HEIGHT_CONTROL", "Invalid height.");
                            Toast.makeText(baseActivity, "Your height can't be less than 40 kilograms.", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    } else {
                        if (Integer.parseInt(editTextsText[item]) < 40) {
                            Log.d("HEIGHT_CONTROL", "Invalid height.");
                            Toast.makeText(baseActivity, "Your height can't be less than 40 kilograms.", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }
                    sharedPreference.setHeight(Float.parseFloat(editTextsText[item]));
                    return true;
                }
            }
        }

        if (profilImagePath != "") {
            sharedPreference.setProfilPhoto(profilImagePath);
        }

        return true;
    }


    private String setProfilImage() {
        String path = sharedPreference.getProfilPhoto();
        Log.i("PROFIL_PHOTO", "GET PHOTO FILE : " + path);
        if (path != "") {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            try {
                imageViewProfilImage_u.setImageBitmap(new BitmapModifyOrientation().modifyOrientation(bitmap, path));
            } catch (IOException e) {
                Toast.makeText(baseActivity, "Measurement error in the photo!", Toast.LENGTH_SHORT).show();
                imageViewProfilImage_u.setImageBitmap(bitmap);
                e.printStackTrace();
            }
        }
        return path;
    }

    @Override
    public void ChangePhoto(String path) {
        Log.i("PHOTO", "getChangePhoto");
        profilImagePath = path;
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        try {
            imageViewProfilImage_u.setImageBitmap(new BitmapModifyOrientation().modifyOrientation(bitmap, path));
        } catch (IOException e) {
            Toast.makeText(baseActivity, "Measurement error in the photo!", Toast.LENGTH_SHORT).show();
            imageViewProfilImage_u.setImageBitmap(bitmap);
            e.printStackTrace();
        }
    }
}
