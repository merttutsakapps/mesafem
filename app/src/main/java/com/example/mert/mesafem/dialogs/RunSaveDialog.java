package com.example.mert.mesafem.dialogs;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.interfaces.OnRunChoiceListener;

/**
 * Created by mert on 18/07/2017.
 */

public class RunSaveDialog extends Dialog {
    private BaseActivity baseActivity;
    private OnRunChoiceListener onRunChoiceListener;

    public RunSaveDialog(@NonNull BaseActivity baseActivity) {
        super(baseActivity);
        this.baseActivity = baseActivity;
    }

    @Override
    public void create() {
        super.create();
        this.setContentView(R.layout.dialog_run_save);

        final Button button_save = (Button) this.findViewById(R.id.buttonPositive);
        final Button button_cancel = (Button) this.findViewById(R.id.buttonNegative);
        final TextView textView_name = (TextView) this.findViewById(R.id.editText_run_name);

        this.setCancelable(true);

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = textView_name.getText().toString();
                if (text.length() > 0) {
                    onRunChoiceListener.onChoice(true, text);
                    dismiss();
                }
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (!this.isShowing()) {
            this.show();
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void setOnRunChoiceListener(OnRunChoiceListener onRunChoiceListener) {
        Log.d(onRunChoiceListener.getClass().getSimpleName(), onRunChoiceListener != null ? "onRunChoiceListener null degil" : "-----null-------");
        this.onRunChoiceListener = onRunChoiceListener;
    }
}
