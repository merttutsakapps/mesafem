package com.example.mert.mesafem.interfaces;

/**
 * Created by mert on 18/07/2017.
 */

public interface OnRunChoiceListener {
    void onChoice(Boolean choice, String name);
}
