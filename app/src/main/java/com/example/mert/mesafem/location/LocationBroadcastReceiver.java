package com.example.mert.mesafem.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.example.mert.mesafem.services.LocationUpdatesService;

public class LocationBroadcastReceiver extends BroadcastReceiver {
    private OnLocationChangeListener onLocationChangeListener = null;
    private Context context;

    public LocationBroadcastReceiver(Context context) {
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);

        if (location != null) {
            if (this.onLocationChangeListener != null) {
                this.onLocationChangeListener.onLocationChange(location);
            } else {
                Log.d(LocationBroadcastReceiver.class.getSimpleName(), "Location : " + location.getLatitude() + " - " + location.getLongitude());
            }
        }
    }

    public void setOnLocationChangeListener(OnLocationChangeListener listener) {
        Log.d(LocationBroadcastReceiver.class.getSimpleName(), listener != null ? "listener null degil" : "-----null-------");
        this.onLocationChangeListener = listener;
    }

    public void clearListener() {
        this.onLocationChangeListener = null;
    }
}

