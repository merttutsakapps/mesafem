package com.example.mert.mesafem.fragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.example.mert.mesafem.R;
import com.example.mert.mesafem.adapters.SampleFragmentPagerAdapter;

import java.util.ArrayList;

public class MainStartFragment extends BaseFragment {
    private PagerSlidingTabStrip tabsStrip;
    private ViewPager viewPager;

    private ArrayList<BaseFragment> fragments;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_start, container, false);

        setPageName("MainStart");
        setHeaderTextActionBar(getResources().getString(R.string.app_name));
        setButtonVisibilityActionBar(false, true);

        fragments = new ArrayList<BaseFragment>();

        fragments.add(new StartFragment());
        fragments.add(new HistoryFragment());

        viewPager = (ViewPager) view.findViewById(R.id.viewpager_start);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getChildFragmentManager(), fragments));

        tabsStrip = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);

        tabsStrip.setViewPager(viewPager);

        return view;
    }
}
