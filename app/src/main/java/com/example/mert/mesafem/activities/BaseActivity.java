package com.example.mert.mesafem.activities;

import android.animation.Animator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.dialogs.CloseDialog;
import com.example.mert.mesafem.dialogs.GpsDialog;
import com.example.mert.mesafem.dialogs.InternetDialog;
import com.example.mert.mesafem.fragments.BaseFragment;
import com.example.mert.mesafem.fragments.ProfilUpdateFragment;
import com.example.mert.mesafem.interfaces.OnChangePhotoListener;
import com.example.mert.mesafem.interfaces.OnRespondListener;
import com.example.mert.mesafem.location.LocationBroadcastReceiver;
import com.example.mert.mesafem.services.LocationUpdatesService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.VISIBLE;

public class BaseActivity extends FragmentActivity implements OnRespondListener {
    public static final int IMAGE_CAPTURE = 1;
    public static final int IMAGE_PICK = 2;

    //ActionBar
    private ImageButton headerLeft;
    private ImageButton headerRight;
    private TextView headerMid;

    private FrameLayout frameLayout;

    private LocationManager locManager;

    private GpsDialog gpsDialog;
    private InternetDialog internetDialog;

    public boolean isExitPage = false;

    private ServiceConnection mServiceConnection;

    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;

    public LocationBroadcastReceiver locationBroadcastReceiver;

    private CloseDialog closeDialog;
    private Bundle bundle;

    private OnChangePhotoListener changePhotoListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        bundle = new Bundle();

        closeDialog = new CloseDialog(BaseActivity.this);

        locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        headerLeft = (ImageButton) findViewById(R.id.header_left);
        headerRight = (ImageButton) findViewById(R.id.header_right);
        headerMid = (TextView) findViewById(R.id.header_text);

        frameLayout = (FrameLayout) findViewById(R.id.main_framelayout);

        headerLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        headerRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new ProfilUpdateFragment());
            }
        });

        locationBroadcastReceiver = new LocationBroadcastReceiver(BaseActivity.this);
        Log.d("HASHCODE_CONTROL_act", "code : " + locationBroadcastReceiver.hashCode());

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
                Log.d("SERVICE", "BAĞLANDI");

                if (mService != null) {
                    Log.d("SERVICE", "LOCATION YENILEDI");
                    mService.requestLocationUpdates();
                } else {
                    Log.d("SERVICE", "LOCATION NULL GELDİ");
                    mService = binder.getService();
                    mService.requestLocationUpdates();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d("SERVICE", "BAĞLANMADI");
            }
        };

        BaseActivity.this.bindService(new Intent(BaseActivity.this, LocationUpdatesService.class), mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(BaseActivity.this).registerReceiver(locationBroadcastReceiver, new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    public void initView(final BaseFragment fragment) {
        if (accessOperation("INIT", fragment)) {
            /*
            getSupportFragmentManager().beginTransaction().replace(R.id.main_framelayout, fragment).commit();
            frameLayout.post(new Runnable() {
                @Override
                public void run() {
                    newPageAnimator(frameLayout).start();
                }
            });*/
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.to_right_animation, R.anim.from_left_animation,R.anim.from_right_animation, R.anim.to_left_animation).replace(R.id.main_framelayout, fragment).commit();
        isExitPage = false;
        }
    }

    public void changeFragment(BaseFragment fragment) {
        if (accessOperation("CHANGE", fragment)) {

            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.to_right_animation, R.anim.from_left_animation,R.anim.from_right_animation, R.anim.to_left_animation).replace(R.id.main_framelayout, fragment).addToBackStack(fragment.toString()).commit();
            /*getSupportFragmentManager().beginTransaction().replace(R.id.main_framelayout, fragment).addToBackStack(fragment.toString()).commit();
            frameLayout.post(new Runnable() {
                @Override
                public void run() {
                    newPageAnimator(frameLayout).start();
                }
            });*/
            isExitPage = false;
            Log.d("STACK", "PUSH => " + fragment.toString());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mService != null) {
            Log.d("SERVICE", "LOCATION YENILEDI");
            mService.requestLocationUpdates();
        } else {
            Log.d("SERVICE", "LOCATION NULL GELDİ");
        }
    }

    @Override
    public void onBackPressed() {
        if (getArguments().getString("pageName", "") == "MainStart" || getArguments().getString("pageName", "") == "Signup") {
            closeDialog.create();
        } else if (accessOperation("BACK", null)) {
            Log.d("STACK", "POP => " + getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1));
            super.onBackPressed();
        }
    }

    public Animator newPageAnimator(View content) {
        final int x = (content.getLeft() + content.getRight()) / 2;
        final int y = (content.getTop() + content.getBottom()) / 2;

        final float finalRounds = (float) Math.max(content.getWidth(), content.getHeight());

        content.setVisibility(View.VISIBLE);

        Animator animator = ViewAnimationUtils.createCircularReveal(content, x, y, 0, finalRounds);
        animator.setDuration(1000);
        return animator;
    }

    public void setArguments(Bundle bundle) {
        Log.d("BUNDLE", "setArguments : " + bundle.toString());
        this.bundle.putAll(bundle);
    }

    public Bundle getArguments() {
        Log.d("BUNDLE", "getArguments : " + this.bundle.toString());
        return this.bundle;
    }

    public void setTextHeadermid(String text) {
        if (text != null) {
            headerMid.setText(text.toUpperCase());
        }
    }

    public void setVisibilityHeaderButton(boolean left, boolean right) {

        if (left == true) {
            headerLeft.setVisibility(VISIBLE);
        } else {
            headerLeft.setVisibility(View.GONE);
        }

        if (right == true) {
            headerRight.setVisibility(VISIBLE);
        } else {
            headerRight.setVisibility(View.GONE);
        }
    }

    public boolean getInfoGPS() {
        if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getInfoInternet() {

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean accessOperation(String choice, BaseFragment fragment) {

        gpsDialog = new GpsDialog(this, choice, fragment);
        internetDialog = new InternetDialog(this, choice, fragment);

        gpsDialog.setOnRespondListener(BaseActivity.this);
        internetDialog.setOnRespondListener(BaseActivity.this);

        if (!getInfoGPS() && !getInfoInternet()) {
            gpsDialog.create();
            internetDialog.create();
            return false;
        } else if (!getInfoGPS() && getInfoInternet()) {
            gpsDialog.create();
            return false;
        } else if (getInfoGPS() && !getInfoInternet()) {
            internetDialog.create();
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_CAPTURE) {
                Log.d("CAMERA", "VERİ GELDİ");
                if (getArguments().getString("profilImagePath", "") != "") {
                    String path = getArguments().getString("profilImagePath");
                    changePhotoListener.ChangePhoto(path);
                }
            } else if (requestCode == IMAGE_PICK) {
                Log.d("GALLERY", "VERİ GELDİ");

                if (data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String path = cursor.getString(columnIndex);
                    changePhotoListener.ChangePhoto(path);
                    cursor.close();
                }
            }
        } else if (requestCode == RESULT_CANCELED) {
            if (requestCode == IMAGE_CAPTURE) {
                Log.d("CAMERA", "VERİ GELDİ. CANCELLED");
                if (getArguments().getString("profilImagePath", "") != "") {
                    new File(getArguments().getString("profilImagePath")).delete();
                }
            }
        } else if (requestCode == IMAGE_PICK) {
            Log.d("GALLERY", "VERİ GELDİ");
        }
    }

    public File createImageFile() throws IOException {
        final File imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + BaseActivity.this.getResources().getString(R.string.app_name));

        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        // Create an image file name
        String imageFileName = "JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date()) + "_";

        File storageDir = imageFile;

        File image = File.createTempFile(
                imageFileName,  // prefix
                ".png",         // suffix
                storageDir      // directory
        );

        return image;
    }

    public void setChangePhotoListener(OnChangePhotoListener changePhotoListener) {
        this.changePhotoListener = changePhotoListener;
    }

    @Override
    public void onChoice(String choice, BaseFragment fragment) {
        Log.i("GPS", "info : " + getInfoGPS());
        Log.i("INTERNET", "info : " + getInfoInternet());

        if (getInfoGPS() && getInfoInternet()) {
            Toast.makeText(this, "Thank you for continuing with us :)", Toast.LENGTH_SHORT).show();
            if (choice == "CHANGE") {
                Log.i("FRAGMENT", fragment.toString());
                if (fragment != null) {
                    changeFragment(fragment);
                }
            } else if (choice == "INIT") {
                Log.i("FRAGMENT", fragment.toString());
                if (fragment != null) {
                    initView(fragment);
                }
            } else if (choice == "BACK") {
                Log.i("FRAGMENT", fragment.toString());
                onBackPressed();
            }
        } else if (!getInfoGPS() || !getInfoInternet()) {

            if (!getInfoInternet()) {
                internetDialog = new InternetDialog(this, choice, fragment);
                internetDialog.setOnRespondListener(BaseActivity.this);
                internetDialog.create();
            } else if (!getInfoGPS()) {
                gpsDialog = new GpsDialog(this, choice, fragment);
                gpsDialog.setOnRespondListener(BaseActivity.this);
                gpsDialog.create();
            }
        }
    }


}
