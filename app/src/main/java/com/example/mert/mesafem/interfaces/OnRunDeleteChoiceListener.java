package com.example.mert.mesafem.interfaces;

/**
 * Created by mert on 28/07/2017.
 */

public interface OnRunDeleteChoiceListener {
    void onDeleteChoice(Boolean choice,String name,int position, long id);
}
