package com.example.mert.mesafem.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.model.Location;
import com.example.mert.mesafem.model.Round;
import com.example.mert.mesafem.model.RoundPage;
import com.example.mert.mesafem.model.Run;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RoundMapFragment extends BaseFragment {
    private MapView mMapView;
    private GoogleMap googleMap;

    public static RoundMapFragment newInstance(Bundle _args) {
        RoundMapFragment roundMapFragment = new RoundMapFragment();
        if (_args != null) {
            roundMapFragment.setArguments(_args);
        }
        return roundMapFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_round_map, container, false);

        setPageName("RondMap");
        setHeaderTextActionBar(getResources().getString(R.string.app_name));
        setButtonVisibilityActionBar(true, false);

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        prepareMap();

        return view;
    }

    private void prepareMap() {
        List<RoundPage> rounds = new ArrayList<>();
        rounds.clear();

        final Bundle args = getArguments();
        if (args.getString("runName") != null) {
            RealmResults<Round> realmListRound = null;

            realmListRound = Realm.getDefaultInstance().where(Run.class).equalTo("name", args.getString("runName")).findFirst().getRounds().sort("id", Sort.ASCENDING);

            realmListRound.load();
            if (realmListRound.isLoaded()) {
                if (!realmListRound.isEmpty()) {
                    progressDialog.onCreateDialog(null);
                    final RealmResults<Round> finalRealmListRound = realmListRound;
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                                if (googleMap != null) {
                                    //CAMERA OBJECTS
                                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                    int padding = 50;
                                    //Map properties
                                    RoundMapFragment.this.googleMap = googleMap;
                                    RoundMapFragment.this.googleMap.setMyLocationEnabled(false);
                                    RoundMapFragment.this.googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                                    List<PolylineOptions> polylines = new ArrayList<>();
                                    List<LatLng> points = new ArrayList<LatLng>();
                                    for (Round r : finalRealmListRound) {
                                        //Lokasyonları belirle
                                        for (int i = 0; i < r.getLocationses().size(); i++) {
                                            builder.include(new LatLng(r.getLocationses().get(i).getLatitude(), r.getLocationses().get(i).getLongitude()));

                                            if (points.size() > 1) {
                                                android.location.Location A = onLocationDefined("Point A", r.getLocationses().get(i));
                                                android.location.Location B = onLocationDefined("Point B", r.getLocationses().get(i - 1));

                                                if (onDistanceCalculation(A, B) >= 0.05) {
                                                    polylines.add(new PolylineOptions().addAll(points).width(10).color(R.color.colorHint).geodesic(true).addAll(points).width(10).color(R.color.colorHint).geodesic(true));
                                                    RoundMapFragment.this.googleMap.addPolyline(polylines.get(polylines.size() - 1));
                                                    points.clear();
                                                }
                                            }
                                            points.add(new LatLng(r.getLocationses().get(i).getLatitude(), r.getLocationses().get(i).getLongitude()));
                                        }
                                        //Lokasyonları çiz
                                        polylines.add(new PolylineOptions().addAll(points).width(10).color(R.color.colorHint).geodesic(true));
                                        RoundMapFragment.this.googleMap.addPolyline(polylines.get(polylines.size() - 1));
                                        points.clear();
                                    }

                                    //First
                                    LatLng first = polylines.get(0).getPoints().get(0);
                                    //Last
                                    LatLng last = polylines.get(polylines.size() - 1).getPoints().get(polylines.get(polylines.size() - 1).getPoints().size() - 1);
                                    //Add Market
                                    RoundMapFragment.this.googleMap.addMarker(new MarkerOptions().anchor(0.5f, 0.55f).snippet("0 km.").position(first).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_002_man)).title("STARTED"));
                                    RoundMapFragment.this.googleMap.addMarker(new MarkerOptions().anchor(0.5f, 0.55f).snippet(String.format("%.2f", args.getDouble("runKilometer")) + " km.").position(last).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_001_victory)).title("FINISHED"));

                                    //Camera Position
                                    LatLngBounds bounds = builder.build();
                                    final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                    RoundMapFragment.this.googleMap.animateCamera(cameraUpdate);
                                    RoundMapFragment.this.googleMap.setMaxZoomPreference(14);
                                    RoundMapFragment.this.googleMap.setMinZoomPreference(10);
                                    progressDialog.dismiss();
                                }
                        }
                    });
                }
            }
        }
    }

    private double onDistanceCalculation(android.location.Location _locationA, android.location.Location _locationB) {
        final double distance = _locationA.distanceTo(_locationB) / 1000;
        Log.i("LOCATION_DISTANCE", String.valueOf(_locationA));
        Log.i("LOCATION_DISTANCE", String.valueOf(_locationB));
        Log.i("LOCATION_DISTANCE", "Distance :" + String.valueOf(distance));
        return distance;
    }

    private android.location.Location onLocationDefined(String _name, Location _location) {
        android.location.Location location = new android.location.Location(_name);
        location.setLatitude(_location.getLatitude());
        location.setLongitude(_location.getLongitude());
        location.setAltitude(_location.getAltitude());
        location.setSpeed(_location.getSpeed());
        location.setAccuracy(_location.getAccuracy());
        location.setBearing(_location.getBearing());
        return location;
    }
}
