package com.example.mert.mesafem.fragments;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.dialogs.RunSaveDialog;
import com.example.mert.mesafem.formats.TimeFormatted;
import com.example.mert.mesafem.interfaces.OnRunChoiceListener;
import com.example.mert.mesafem.location.OnLocationChangeListener;
import com.example.mert.mesafem.model.Location;
import com.example.mert.mesafem.model.Round;
import com.example.mert.mesafem.model.Run;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class PlayFragment extends BaseFragment implements OnLocationChangeListener, OnRunChoiceListener {

    private TextView textViewCountDown;
    private ProgressBar progressBarCountDown;
    private CountDownTimer countDownTimer;

    private TextView textViewPace;
    private TextView textViewkilometers;
    private Chronometer playChronometer;

    private ImageButton imageButtonPause;
    private ImageButton imageButtonStop;
    private ImageButton imageButtonStart;

    private RelativeLayout relativelayoutCountDown;
    private RelativeLayout relativelayoutPlay;

    private long timeWhenStopped = 0;
    private boolean isStartChronometer = false;

    private double sumDistance = 0.0;
    private double runControlValue = 0.005;
    private double stopControl = runControlValue;
    private int lastLocationNumber;
    private int lastRoundNumber;
    private int firstRoundId;
    private long lastTimeOfChronometer = 0;

    private RunSaveDialog runSaveDialog;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_play, container, false);

        setPageName("Play");
        setHeaderTextActionBar(getResources().getString(R.string.app_name));
        setButtonVisibilityActionBar(false, false);
        baseActivity.locationBroadcastReceiver.setOnLocationChangeListener(PlayFragment.this);

        sumDistance = 0.0;
        stopControl = 0.0;

        runSaveDialog = new RunSaveDialog(baseActivity);
        runSaveDialog.setOnRunChoiceListener(PlayFragment.this);

        progressBarCountDown = (ProgressBar) view.findViewById(R.id.progressBar_count_down);

        textViewCountDown = (TextView) view.findViewById(R.id.textView_count_down);
        textViewPace = (TextView) view.findViewById(R.id.textView_pace);
        textViewkilometers = (TextView) view.findViewById(R.id.textView_kilometers);
        playChronometer = (Chronometer) view.findViewById(R.id.play_chronometer);
        imageButtonPause = (ImageButton) view.findViewById(R.id.imageButton_pause);
        imageButtonStop = (ImageButton) view.findViewById(R.id.imageButton_stop);
        imageButtonStart = (ImageButton) view.findViewById(R.id.imageButton_start);

        relativelayoutCountDown = (RelativeLayout) view.findViewById(R.id.relativelayout_count_down);
        relativelayoutPlay = (RelativeLayout) view.findViewById(R.id.relativelayout_play);

        relativelayoutCountDown.setVisibility(View.VISIBLE);
        relativelayoutPlay.setVisibility(View.GONE);

        playChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                lastTimeOfChronometer = SystemClock.elapsedRealtime() - chronometer.getBase();
                Log.i("CHRONOMETER", "long: " + String.valueOf(lastTimeOfChronometer) + " Time :" + new TimeFormatted().convertTimeToHour(lastTimeOfChronometer));
            }
        });

        PlayCountDown();

        imageButtonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isStartChronometer = false;

                if (!Realm.getDefaultInstance().where(Round.class).equalTo("roundId", lastRoundNumber).findAll().isEmpty()) {
                    runSaveDialog.create();
                } else {
                    Toast.makeText(baseActivity, "No record was taken because of there wasn't enough road taken.", Toast.LENGTH_LONG).show();
                    backPressButton();
                }
            }
        });

        imageButtonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeWhenStopped = playChronometer.getBase() - SystemClock.elapsedRealtime();
                playChronometer.stop();
                setButtonVisibilityActionBar(true, false);

                imageButtonPause.setVisibility(View.GONE);
                imageButtonStart.setVisibility(View.VISIBLE);
                imageButtonStop.setVisibility(View.VISIBLE);

                isStartChronometer = false;

                if (sumDistance >= runControlValue) {
                    saveRoundToDatabase(sumDistance);
                }
                lastLocationNumber = newLocationNumber();
            }
        });

        imageButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setButtonVisibilityActionBar(false, false);

                imageButtonPause.setVisibility(View.VISIBLE);
                imageButtonStart.setVisibility(View.GONE);
                imageButtonStop.setVisibility(View.GONE);

                playChronometer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
                playChronometer.start();
                isStartChronometer = true;
            }
        });

        return view;
    }

    private void PlayCountDown() {
        countDownTimer = new CountDownTimer(4000, 10) {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onTick(long millisUntilFinished) {
                final int seconds = (int) (millisUntilFinished / 1000);
                progressBarCountDown.setProgress((int) (millisUntilFinished - 1000));

                if (seconds < 1) {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            textViewCountDown.setTextColor(getResources().getColor(R.color.colorApp, null));
                        }
                        textViewCountDown.setTextSize(100);
                        textViewCountDown.setText("START");
                    } catch (IllegalStateException e) {}
                } else {
                    try {
                        textViewCountDown.setTextColor(getResources().getColor(R.color.colorHint, null));
                        textViewCountDown.setText(String.valueOf(seconds));
                    } catch (IllegalStateException e) {}
                }
            }

            @Override
            public void onFinish() {
                relativelayoutCountDown.setVisibility(View.GONE);
                imageButtonPause.setVisibility(View.VISIBLE);
                imageButtonStart.setVisibility(View.GONE);
                imageButtonStop.setVisibility(View.GONE);
                relativelayoutPlay.setVisibility(View.VISIBLE);

                Log.d("ROUND_REALM", "Round.isValid : " + Realm.getDefaultInstance().where(Round.class).isValid());
                if (Realm.getDefaultInstance().where(Round.class).isValid() == true) {
                    Log.d("ROUND_REALM", "Round.isEmpty : " + Realm.getDefaultInstance().where(Round.class).findAll().isEmpty());
                    if (Realm.getDefaultInstance().where(Round.class).findAll().isEmpty() == true) {

                        lastRoundNumber = 1;
                        firstRoundId = lastRoundNumber;
                        Log.d("ROUND_REALM", "lastRoundNumber : " + lastRoundNumber);

                        Log.d("LOCATION_REALM", "Location.isEmpty : " + Realm.getDefaultInstance().where(Location.class).findAll().isEmpty());
                        if (Realm.getDefaultInstance().where(Round.class).findAll().isEmpty() == true) {
                            lastLocationNumber = 1;
                            Log.d("LOCATION_REALM", "lastLocationNumber : " + lastLocationNumber);
                        } else {
                            Log.d("LOCATION_REALM", "Location.max('id') : " + Realm.getDefaultInstance().where(Location.class).max("id").intValue());
                            lastLocationNumber = Realm.getDefaultInstance().where(Location.class).max("id").intValue() + 1;
                        }
                    } else {
                        lastRoundNumber = newRoundNumber();
                        firstRoundId = lastRoundNumber;
                        Log.d("LOCATION_REALM", "Location.isEmpty : " + Realm.getDefaultInstance().where(Location.class).findAll().isEmpty());
                        if (Realm.getDefaultInstance().where(Round.class).findAll().isEmpty() == true) {
                            lastLocationNumber = 1;
                            Log.d("LOCATION_REALM", "lastLocationNumber : " + lastLocationNumber);
                        } else {
                            Log.d("LOCATION_REALM", "Location.max('id') : " + Realm.getDefaultInstance().where(Location.class).max("id").intValue());
                            lastLocationNumber = Realm.getDefaultInstance().where(Location.class).max("id").intValue() + 1;
                        }
                    }
                }
                playChronometer.setBase(SystemClock.elapsedRealtime());
                playChronometer.start();
                isStartChronometer = true;
            }
        };
        countDownTimer.start();
    }

    @Override
    public void onLocationChange(android.location.Location location) {
        Log.d("CHRONOMETER", String.valueOf(isStartChronometer));
        if (isStartChronometer) {
            saveLocationToDatabase(location);
        }
    }

    private void saveLocationToDatabase(final android.location.Location _location) {
        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Number currentId = Realm.getDefaultInstance().where(Location.class).max("id");

                int nextId;
                if (currentId == null) {
                    nextId = 1;
                } else {
                    nextId = currentId.intValue() + 1;
                }
                final Location location = Realm.getDefaultInstance().createObject(Location.class, nextId);

                location.setLatitude(_location.getLatitude());
                location.setLongitude(_location.getLongitude());
                location.setAccuracy(_location.getAccuracy());
                location.setAltitude(_location.getAltitude());
                location.setBearing(_location.getBearing());
                location.setSpeed(_location.getSpeed());
                location.setCreatedAt(new Date());
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.d("LOCATON_REALM", "locations : ADD ITEM TO DATABASE");

                Log.d("LOCATON_REALM", "Location.greaterThanOrEqualTo('id', lastLocationNumber).count() :" + Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).count());
                if (Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).count() % 2 == 0) {

                    RealmResults<Location> distanceResult = Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).findAllSorted("id", Sort.DESCENDING);
                    distanceResult.load();
                    Log.d("LOCATION_REALM", distanceResult.toString());

                    android.location.Location locationA = onLocationDefined("point A", distanceResult.get(0));

                    android.location.Location locationB = null;

                    if (Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).count() == 2) {
                        locationB = onLocationDefined("point B", distanceResult.get(1));
                    } else if (Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).count() > 2) {
                        locationB = onLocationDefined("point B", distanceResult.get(2));
                    }

                    //DISTANCE
                    double distance = onDistanceCalculation(locationA, locationB);

                    sumDistance += distance;
                    Log.i("LOCATION_DISTANCE", "sumDistance :" + String.valueOf(sumDistance));

                    if (Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).count() % 2 == 0) {
                        android.location.Location locationC = locationB;
                        stopControl = onDistanceCalculation(locationA, locationC);
                        Log.i("LOCATION_DISTANCE", "stopControl :" + String.valueOf(distance) + " => distance :" + distance + " => sumDistance :" + sumDistance);
                    } else {
                        stopControl = runControlValue;
                        Log.i("LOCATION_DISTANCE", "stopControl :" + String.valueOf(distance) + " => distance :" + distance + " => sumDistance :" + sumDistance);
                    }

                    if (sumDistance >= runControlValue && stopControl >= runControlValue) {//devam ediyor
                        Log.i("distance", "stop control active -- MOVEMENT");
                        if (distance <= 0.08) {
                            //Log.i("LOCATION_DISTANCE", "The distance is less than 0.07 kilometers -> " + distance);
                            Log.i("LOCATION_DISTANCE", "Your speed is less than 35 km/s -> " + locationA.getSpeed());
                            Log.i("LOCATION_REALM", Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).findAll().last().toString());

                            if (((sumDistance - distance) < Math.floor(sumDistance)) && ((sumDistance - distance) > 0.0)) {
                                Toast.makeText(baseActivity, String.valueOf(Math.floor(sumDistance)) + ". KM", Toast.LENGTH_LONG).show();

                                saveRoundToDatabase(1.0);
                                lastRoundNumber = newRoundNumber();

                                textViewPace.setText(new TimeFormatted().convertPace((long) Realm.getDefaultInstance().where(Round.class).greaterThanOrEqualTo("roundId", firstRoundId).findAll().average("pace")));
                            }
                        } else {
                            //Log.i("LOCATION_DISTANCE", "The distance is too much -> " + sumDistance);
                            Log.i("LOCATION_DISTANCE", "Your speed is too much -> " + locationA.getSpeed());
                            sumDistance -= distance;
                            imageButtonPause.performClick();
                            Toast.makeText(baseActivity, "The device is moving too fast !", Toast.LENGTH_LONG).show();

                        }
                        textViewkilometers.setText(String.format("%.2f", sumDistance));
                    } else if (sumDistance >= runControlValue && stopControl < runControlValue) {//durmuş
                        Log.i("distance", "stop control active -- STOPPED");
                        imageButtonPause.performClick();
                        Toast.makeText(baseActivity, "The device stopped because there was not enough movement has been achieved.", Toast.LENGTH_LONG).show();
                    } else if (sumDistance < runControlValue && stopControl <= runControlValue) {//hareket yok(en başta başlamasına rağmen hareket etmiyorsa)
                        Log.i("distance", "stop control active -- ANY MOVE");
                        //PAUSE EVENT
                        timeWhenStopped = playChronometer.getBase() - SystemClock.elapsedRealtime();
                        playChronometer.stop();
                        setButtonVisibilityActionBar(true, false);

                        imageButtonPause.setVisibility(View.GONE);
                        imageButtonStart.setVisibility(View.VISIBLE);
                        imageButtonStop.setVisibility(View.VISIBLE);

                        playChronometer.setBase(SystemClock.elapsedRealtime());
                        timeWhenStopped = 0;
                        isStartChronometer = false;
                        Toast.makeText(baseActivity, "not enough movement has been achieved.", Toast.LENGTH_LONG).show();
                    } else {//hatalı işlem
                        sumDistance -= distance;
                        Toast.makeText(baseActivity, "Incorrect process!", Toast.LENGTH_SHORT);

                        if (sumDistance < 0) {
                            sumDistance = 0;
                        }
                    }
                }

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.d("LOCATION_REALM", "locations : NOT ADD ITEM TO DATABASE :" + error.getMessage());
            }
        });
    }

    private void saveRoundToDatabase(double _sumDistance) {
        long _pace = 0;
        long _time = 0;
        double _distance = 0;
        Log.d("ROUND_REALM", "roundId =" + lastRoundNumber + " -> isEmpty : " + Realm.getDefaultInstance().where(Round.class).equalTo("roundId", lastRoundNumber).findAll().isEmpty());

        if (lastRoundNumber == firstRoundId) {
            _time = lastTimeOfChronometer;
            if (_sumDistance == 1) {
                _distance = 1;
            } else {
                _distance = _sumDistance;
            }
            _pace = onPaceCalculation(lastTimeOfChronometer, _distance);
            Log.i("LOCATION_DISTANCE", "pace : " + _pace);
        } else {
            Round beforeRound = Realm.getDefaultInstance().where(Round.class).equalTo("roundId", lastRoundNumber - 1).findFirst();
            long last_pace = beforeRound.getTime();
            double _km = (Double) Realm.getDefaultInstance().where(Round.class).greaterThanOrEqualTo("roundId", firstRoundId).lessThanOrEqualTo("roundId", lastRoundNumber - 1).sum("kilometers");
            if (_sumDistance == 1) {
                _distance = 1;
            } else {
                _distance = _sumDistance - _km;
            }
            _time = lastTimeOfChronometer;
            _pace = onPaceCalculation((_time - last_pace), _km);
            Log.i("LOCATION_DISTANCE", "pace : " + _pace);
        }

        //EKLEME İŞLEMİ
        Realm.getDefaultInstance().beginTransaction();

        final RealmResults<Location> managedLocation = Realm.getDefaultInstance().where(Location.class).greaterThanOrEqualTo("id", lastLocationNumber).findAll();

        Number currentId = Realm.getDefaultInstance().where(Round.class).max("id");
        int nextId;

        if (currentId == null) {
            nextId = 1;
        } else {
            nextId = currentId.intValue() + 1;
        }

        final RealmResults<Round> rounds = Realm.getDefaultInstance().where(Round.class).equalTo("roundId", lastRoundNumber).findAll();

        if (rounds.isEmpty()) {

            final Round round = Realm.getDefaultInstance().createObject(Round.class, nextId);
            round.setRoundId(lastRoundNumber);
            round.setTime(_time);
            round.setKilometers(_distance);
            round.setPace(_pace);
            do {
                managedLocation.load();
                if (managedLocation.isLoaded()) {
                    for (Location l : managedLocation) {
                        round.getLocationses().add(l);
                    }
                }
            } while (!managedLocation.isLoaded());
            round.setCreatedAt(new Date());
        } else {
            final Round round = Realm.getDefaultInstance().where(Round.class).equalTo("roundId", lastRoundNumber).findFirst();
            round.setRoundId(lastRoundNumber);
            round.setTime(_time);
            round.setKilometers(_distance);
            round.setPace(_pace);
            do {
                managedLocation.load();
                if (managedLocation.isLoaded()) {
                    for (Location l : managedLocation) {
                        round.getLocationses().add(l);
                    }
                }
            } while (!managedLocation.isLoaded());
            round.setCreatedAt(new Date());

            Realm.getDefaultInstance().insertOrUpdate(round);
        }
        Realm.getDefaultInstance().commitTransaction();
        Log.i("ROUND_REALM", Realm.getDefaultInstance().where(Round.class).findAll().last().toString());
    }

    private void saveRunToDatabase(String name) {
        Realm.getDefaultInstance().beginTransaction();

        final RealmResults<Round> managedRound = Realm.getDefaultInstance().where(Round.class).greaterThanOrEqualTo("roundId", firstRoundId).findAll();

        Number currentId = Realm.getDefaultInstance().where(Run.class).max("id");
        int nextId;

        if (currentId == null) {
            nextId = 1;
        } else {
            nextId = currentId.intValue() + 1;
        }

        final Run run = Realm.getDefaultInstance().createObject(Run.class, nextId);

        do {
            managedRound.load();
            if (managedRound.isLoaded()) {
                for (Round r : managedRound) {
                    run.getRounds().add(r);
                }
            }
        } while (!managedRound.isLoaded());
        run.setName(name);

        Realm.getDefaultInstance().commitTransaction();

        Log.i("RUN_REALM", Realm.getDefaultInstance().where(Run.class).findAll().last().toString());
    }

    private long onPaceCalculation(long _time, double _km) {
        long _pace = (long) (_time / _km);
        Log.i("PACE_CALCULATION", "pace : " + new TimeFormatted().convertPace(_pace) + " km :" + _km + " time :" + new TimeFormatted().convertTimeToHour(_time));
        return _pace;
    }

    private double onDistanceCalculation(android.location.Location _locationA, android.location.Location _locationB) {

        final double distance = _locationA.distanceTo(_locationB) / 1000;

        Log.i("LOCATION_DISTANCE", String.valueOf(_locationA));
        Log.i("LOCATION_DISTANCE", String.valueOf(_locationB));
        Log.i("LOCATION_DISTANCE", "Distance :" + String.valueOf(distance));

        return distance;

    }

    private android.location.Location onLocationDefined(String _name, Location _location) {
        android.location.Location location = new android.location.Location(_name);

        location.setLatitude(_location.getLatitude());
        location.setLongitude(_location.getLongitude());
        location.setAltitude(_location.getAltitude());
        location.setSpeed(_location.getSpeed());
        location.setAccuracy(_location.getAccuracy());
        location.setBearing(_location.getBearing());

        return location;
    }

    private int newLocationNumber() {
        Log.d("LOCATION_REALM", "Location.max('id') : " + Realm.getDefaultInstance().where(Location.class).max("id").intValue());
        return Realm.getDefaultInstance().where(Location.class).max("id").intValue() + 1;
    }

    private int newRoundNumber() {
        Log.d("ROUND_REALM", "Round.max('roundId') : " + Realm.getDefaultInstance().where(Round.class).max("roundId").intValue());
        return Realm.getDefaultInstance().where(Round.class).max("roundId").intValue() + 1;
    }

    @Override
    public void onChoice(Boolean choice, String name) {
        if (choice) {
            runSaveDialog.dismiss();
            saveRunToDatabase(name);
            backPressButton();
        }
    }
}