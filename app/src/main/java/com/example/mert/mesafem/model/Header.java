package com.example.mert.mesafem.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by mert on 12/07/2017.
 */

public class Header {

    private String HeaderName;
    private List<Run> runs;

    public List<Run> getRuns() {
        return runs;
    }

    public void setRuns(List<Run> runs) {
        this.runs = runs;
    }

    public String getHeaderName() {
        return HeaderName;
    }

    public void setHeaderName(String HeaderName) {
        this.HeaderName = HeaderName;
    }

    public void addRun(Run run) {
        if (runs == null) {
            runs = new ArrayList<>();
        }

        runs.add(run);
    }
}
