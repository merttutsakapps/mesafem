package com.example.mert.mesafem;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Mesafem extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        String roothPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfig);
        Log.d("ngedebug", "Init Realm");
    }
}
