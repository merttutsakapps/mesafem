package com.example.mert.mesafem.LocationManager;

import java.util.List;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.permissions.RequestPermissions;


public class CustomLocationManager {

    private static final long UZAKLIK_DEGISIMI = 0;
    private static final long SURE = 1000 * 30;

    private Location mMevcutKonum;
    private LocationManager mLocationManager;
    private KonumListener mKonumListener;
    private BaseActivity baseActivity;

    private RequestPermissions requestPermissions;

    public CustomLocationManager(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;

        requestPermissions = new RequestPermissions(baseActivity);

        mLocationManager = (LocationManager)
                this.baseActivity.getSystemService(Context.LOCATION_SERVICE);

        mKonumListener = new KonumListener();
    }


    public void baslaKonumGuncellemesi() {
        Criteria kriter = new Criteria();
        kriter.setAccuracy(Criteria.ACCURACY_COARSE);
        kriter.setAltitudeRequired(false);
        kriter.setSpeedRequired(false);
        kriter.setPowerRequirement(Criteria.POWER_MEDIUM);
        kriter.setCostAllowed(false);
        String bilgiSaglayici =
                mLocationManager.getBestProvider(kriter, true);
        if (bilgiSaglayici == null) {
            List<String> bilgiSaglayicilar =
                    mLocationManager.getAllProviders();
            for (String tempSaglayici : bilgiSaglayicilar) {
                if (mLocationManager.isProviderEnabled(tempSaglayici))
                    bilgiSaglayici = tempSaglayici;
            }
        }

        //kriterlere uyan bir LocationManager al
        if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions.requestPermissions();
        }
        mLocationManager.requestLocationUpdates(
                bilgiSaglayici,
                SURE,
                UZAKLIK_DEGISIMI,
                mKonumListener);
    }

    public void durdurKonumGuncellemesi() {
        if (mLocationManager != null)
            mLocationManager.removeUpdates(mKonumListener);
    }

    public Location getMevcutKonum() {
        return mMevcutKonum;
    }

    //Inner class - Dahili sınıf
    private class KonumListener implements LocationListener {
        public void onLocationChanged(Location location) {
            mMevcutKonum = location;
        }

        public void onStatusChanged(String provider,
                                    int status, Bundle extras) {
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }
    }
}
