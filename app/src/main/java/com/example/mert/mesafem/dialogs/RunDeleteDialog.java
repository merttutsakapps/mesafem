package com.example.mert.mesafem.dialogs;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.interfaces.OnRunDeleteChoiceListener;

/**
 * Created by mert on 18/07/2017.
 */

public class RunDeleteDialog extends Dialog {
    private BaseActivity baseActivity;
    private OnRunDeleteChoiceListener onRunDeleteChoiceListener;

    private int position = 0;
    private long id = 0;
    private String name = "";

    public RunDeleteDialog(@NonNull BaseActivity baseActivity, String name, int position, long id) {
        super(baseActivity);
        this.baseActivity = baseActivity;
        this.position = position;
        this.id = id;
        this.name = name;
    }

    @Override
    public void create() {
        super.create();
        this.setContentView(R.layout.dialog_run_delete);

        final Button button_delete = (Button) this.findViewById(R.id.buttonDelete);
        final Button button_cancel = (Button) this.findViewById(R.id.buttonCancel);
        final TextView textView_name = (TextView) this.findViewById(R.id.textView_run_name);

        if (name != "" || name != null) {
            textView_name.setText("Are you sure you want to delete " + name.toUpperCase() + " RUN ?");
        } else {
            textView_name.setText("Are you sure you want to delete the run ?");
        }

        this.setCancelable(false);

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRunDeleteChoiceListener.onDeleteChoice(true, name, position, id);
                dismiss();
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRunDeleteChoiceListener.onDeleteChoice(false, name, position, id);
                dismiss();
            }
        });

        if (!this.isShowing()) {
            this.show();
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void setOnRunDeleteChoiceListener(OnRunDeleteChoiceListener onRunDeleteChoiceListener) {
        Log.d(onRunDeleteChoiceListener.getClass().getSimpleName(), onRunDeleteChoiceListener != null ? "onRunDeleteChoiceListener null degil" : "-----null-------");
        this.onRunDeleteChoiceListener = onRunDeleteChoiceListener;
    }
}
