package com.example.mert.mesafem.interfaces;

import com.example.mert.mesafem.fragments.BaseFragment;

/**
 * Created by mert on 18/07/2017.
 */

public interface OnRespondListener {
    void onChoice(String choice, BaseFragment fragment);
}
