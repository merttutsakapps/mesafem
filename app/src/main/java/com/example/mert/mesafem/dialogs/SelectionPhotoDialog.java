package com.example.mert.mesafem.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.interfaces.OnChangePhotoListener;
import com.example.mert.mesafem.preferences.SharedPreference;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SelectionPhotoDialog extends Dialog {
    public static final int IMAGE_CAPTURE = 1;
    public static final int IMAGE_PICK = 2;

    private BaseActivity baseActivity;
    private Button buttonCameraSelect;
    private Button buttonGalleryCheck;

    private Intent galleryIntent;
    private File imageFile;


    public SelectionPhotoDialog(BaseActivity baseActivity) {
        super(baseActivity);
        this.baseActivity = baseActivity;
    }

    @Override
    public void create() {
        super.create();
        this.setContentView(R.layout.dialog_selection_photo);

        buttonCameraSelect = (Button) this.findViewById(R.id.button_camera_select);
        buttonGalleryCheck = (Button) this.findViewById(R.id.button_gallery_select);

        this.setCancelable(true);

        buttonCameraSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CameraSelection();
                dismiss();
            }
        });

        buttonGalleryCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GalleryCheck();
                dismiss();
            }
        });

        if (!this.isShowing()) {
            this.show();
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }


    private void CameraSelection() {
        dismiss();
        Log.d("CAMERA", "OPEN");
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            imageFile = baseActivity.createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String path = imageFile.getAbsolutePath();

        Log.d("PROFIL_PHOTO", "Created file : " + path);
        Bundle bundle = new Bundle();
        bundle.putString("profilImagePath", path);
        baseActivity.setArguments(bundle);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
        baseActivity.startActivityForResult(cameraIntent, IMAGE_CAPTURE);
        Log.d("CAMERA", "ACTION RESULT");
    }

    private void GalleryCheck() {
        dismiss();
        Log.d("GALLERY", "OPEN");
        galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        baseActivity.startActivityForResult(Intent.createChooser(galleryIntent, "Select a photo!"), IMAGE_PICK);
    }
}
