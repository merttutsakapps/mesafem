package com.example.mert.mesafem.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.example.mert.mesafem.activities.BaseActivity;

public class SharedPreference {

    private BaseActivity baseActivity;
    private SharedPreferences preferencesProfil;
    private SharedPreferences.Editor editor;

    public SharedPreference(BaseActivity baseActivity) {

        this.baseActivity = baseActivity;

        preferencesProfil = this.baseActivity.getSharedPreferences("profil", Context.MODE_PRIVATE);
        editor = preferencesProfil.edit();
    }


    public void setFirstName(String firstName) {
        String firstChar = firstName.substring(0, 1);
        String lastChar = firstName.substring(1);
        editor.putString("FIRST_NAME", firstChar.toUpperCase() + lastChar.toLowerCase()
        ).commit();
    }

    public String getFirstName() {
        return preferencesProfil.getString("FIRST_NAME", "");
    }

    public void setLastName(String lastName) {
        editor.putString("LAST_NAME", lastName.toUpperCase()).commit();
    }

    public String getLastName() {
        return preferencesProfil.getString("LAST_NAME", "");
    }

    public void setEMail(String eMail) {
        editor.putString("E_MAIL", eMail).commit();
    }

    public String getEMail() {
        return preferencesProfil.getString("E_MAIL", "");
    }

    public void setHeight(float height) {
        editor.putFloat("HEIGHT", height).commit();
    }

    public float getHeight() {
        return preferencesProfil.getFloat("HEIGHT", 0.0f);
    }

    public void setWeight(float weight) {
        editor.putFloat("WEIGHT", weight).commit();
    }

    public float getWeight() {
        return preferencesProfil.getFloat("WEIGHT", 0.0f);
    }

    public void setProfilPhoto(String data) {
        editor.putString("PROFIL_IMAGE", data).apply();
    }

    public String getProfilPhoto() {
            return preferencesProfil.getString("PROFIL_IMAGE", "");
    }

    public void setProfil(String firstName, String lastName, String eMail, float height, float weight, String imageProfil) {

        setFirstName(firstName);
        setLastName(lastName);
        setEMail(eMail);
        setHeight(height);
        setWeight(weight);
        setProfilPhoto(imageProfil);
    }

    public boolean isEmpty() {
        if (getFirstName() != "" && getLastName() != "" && getEMail() != "") {
            return false;
        } else {
            return true;
        }
    }
}
