package com.example.mert.mesafem.location;

import android.location.Location;

public interface OnLocationChangeListener {

    void onLocationChange(Location location);
}