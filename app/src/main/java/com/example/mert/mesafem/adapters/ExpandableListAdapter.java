package com.example.mert.mesafem.adapters;

import android.os.Build;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.formats.TimeFormatted;
import com.example.mert.mesafem.model.Header;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.realm.Sort;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private BaseActivity baseActivity;
    private List<Header> headers;

    public ExpandableListAdapter(BaseActivity baseActivity, List<Header> headers) {
        super();
        this.baseActivity = baseActivity;
        this.headers = headers;
    }

    @Override
    public int getGroupCount() {
        return headers != null ? headers
                .size() : 0;
        //return headers.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return headers.get(groupPosition).getRuns().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return headers.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        return headers.get(groupPosition).getRuns().get(childPosititon);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) baseActivity.getSystemService(baseActivity.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.listview_header, null);
        }
        TextView textViewHeader = (TextView) convertView.findViewById(R.id.textView_headline);
        textViewHeader.setText(String.valueOf(headers.get(groupPosition).getHeaderName()));
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView_row_state);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isExpanded) {
                imageView.setImageDrawable(baseActivity.getResources().getDrawable(R.drawable.expand_row, null));
            } else {
                imageView.setImageDrawable(baseActivity.getResources().getDrawable(R.drawable.collapse_row, null));
            }
        } else {
            if (isExpanded) {
                imageView.setImageDrawable(baseActivity.getResources().getDrawable(R.drawable.expand_row));
            } else {
                imageView.setImageDrawable(baseActivity.getResources().getDrawable(R.drawable.collapse_row));
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Log.i("Expandable", "group :" + groupPosition + "chlid :" + childPosition + " --- " + headers.get(groupPosition).getRuns().get(childPosition).toString());

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) baseActivity.getSystemService(baseActivity.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.listview_child, null);
        }

        TextView textViewDate = (TextView) convertView.findViewById(R.id.textView_list_date);
        TextView textViewRunName = (TextView) convertView.findViewById(R.id.textView_list_run_name);
        TextView textViewKilometers = (TextView) convertView.findViewById(R.id.textView_list_kilometers);
        TextView textViewPace = (TextView) convertView.findViewById(R.id.textView_list_pace);
        TextView textViewSumTime = (TextView) convertView.findViewById(R.id.textView_list_sum_time);

        //Date
        String createdAt = DateFormat.format("cc/MMMM/yyyy H:mm", new Date(headers.get(groupPosition).getRuns().get(childPosition).getCreatedAt().getTime())).toString();
        //Kilometers
        String sumKilometers = String.format("%.2f", headers.get(groupPosition).getRuns().get(childPosition).getRounds().sum("kilometers"));
        //Pace
        String avrPace = new TimeFormatted().convertPace((long) headers.get(groupPosition).getRuns().get(childPosition).getRounds().average("pace"));
        //SumTime

        textViewDate.setText(createdAt);
        textViewRunName.setText(headers.get(groupPosition).getRuns().get(childPosition).getName().toUpperCase());
        textViewKilometers.setText(sumKilometers);
        textViewPace.setText(avrPace);
        if (headers.get(groupPosition).getRuns().get(childPosition).getRounds().isEmpty() == false) {
            long _time = headers.get(groupPosition).getRuns().get(childPosition).getRounds().sort("id", Sort.ASCENDING).last().getTime();
            textViewSumTime.setText(new TimeFormatted().convertTimeToHour(_time));
        } else {
            textViewSumTime.setText("--:--:--");
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    public List<Header> getHeaders() {
        return headers;
    }

}
