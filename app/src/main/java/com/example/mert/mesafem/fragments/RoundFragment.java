package com.example.mert.mesafem.fragments;

import android.Manifest;
import android.animation.Animator;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.adapters.RoundListAdapter;
import com.example.mert.mesafem.model.Location;
import com.example.mert.mesafem.model.Round;
import com.example.mert.mesafem.model.RoundPage;
import com.example.mert.mesafem.model.Run;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class RoundFragment extends BaseFragment {
    private Fragment fragment;
    private MapView mMapView;
    private GoogleMap googleMap;

    private RoundListAdapter roundListAdapter;
    private ListView listViewRounds;
    private List<RoundPage> roundPageList;

    private TextView textViewTag;
    private TextView textViewSumKM;
    private TextView textViewDate;
    private TextView textViewAvrPace;
    private TextView textViewSumTime;
    private TextView textViewNoRecord;
    private RelativeLayout filigram;
    private LinearLayout linearLayoutList;

    private String runName = "";

    public static RoundFragment newInstance(Bundle _args) {
        RoundFragment roundFragment = new RoundFragment();
        if (_args != null) {
            roundFragment.setArguments(_args);
        }
        return roundFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_round, container, false);

        setPageName("Round");
        setHeaderTextActionBar(getResources().getString(R.string.app_name));
        setButtonVisibilityActionBar(true, false);

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        listViewRounds = (ListView) view.findViewById(R.id.listView_round);
        filigram = (RelativeLayout) view.findViewById(R.id.filigram);
        textViewTag = (TextView) view.findViewById(R.id.textView_tag);
        textViewSumKM = (TextView) view.findViewById(R.id.textView_filigram_kilometers);
        textViewDate = (TextView) view.findViewById(R.id.textView_filigram_date);
        textViewAvrPace = (TextView) view.findViewById(R.id.textView_avr_pace);
        textViewSumTime = (TextView) view.findViewById(R.id.textView_sum_time);

        textViewNoRecord = (TextView) view.findViewById(R.id.textView_no_record);
        linearLayoutList = (LinearLayout) view.findViewById(R.id.linearLayout_list);

        //ICERIGI DOLDUR.
        readBundle();

        if (linearLayoutList.getVisibility() == View.VISIBLE) {
            //LİST DOLDUR
            roundPageList = prepareListData();
            roundListAdapter = new RoundListAdapter(baseActivity, roundPageList);
            listViewRounds.setAdapter(roundListAdapter);
        }

        //Haritada yol göster
        prepareMap();

        filigram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putDouble("runKilometer", getArguments().getDouble("runKilometer"));
                args.putString("runName", getArguments().getString("runName"));
                replaceFragment(new RoundMapFragment().newInstance(args));

            }
        });
        return view;
    }

    private List<RoundPage> prepareListData() {
        List<RoundPage> rounds = new ArrayList<>();
        rounds.clear();

        RealmResults<Round> realmListRound = Realm.getDefaultInstance().where(Run.class).equalTo("name", runName).findFirst().getRounds().sort("id", Sort.ASCENDING);
        realmListRound.load();

        Log.i("ROUNDPAGE", "realmListRound.isEmpty : " + realmListRound.isEmpty());

        if (!realmListRound.isEmpty()) {

            textViewNoRecord.setVisibility(View.GONE);
            listViewRounds.setVisibility(View.VISIBLE);

            Log.i("ROUNDPAGE", "realmListRound.size = " + realmListRound.size());
            for (int i = 0; i < realmListRound.size(); i++) {
                RoundPage roundPage = new RoundPage();
                roundPage.setRound(realmListRound.get(i));
                rounds.add(roundPage);
            }
        }
        return rounds;
    }

    private void prepareMap() {
        final List<RoundPage> rounds = new ArrayList<>();
        rounds.clear();

        final Bundle args = getArguments();

        if (args.getString("runName") != null) {
            RealmResults<Round> realmListRound = null;

            try {
                realmListRound = Realm.getDefaultInstance().where(Run.class).equalTo("name", args.getString("runName")).findFirst().getRounds().sort("id", Sort.ASCENDING);
                realmListRound.load();
            } catch (Exception e) {
            }

            if (realmListRound.isLoaded()) {
                if (!realmListRound.isEmpty()) {
                    progressDialog.onCreateDialog(null);

                    final RealmResults<Round> finalRealmListRound = realmListRound;
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                if (googleMap != null) {
                                    //CAMERA OBJECTS
                                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                    int padding = 100;
                                    //Map properties
                                    RoundFragment.this.googleMap = googleMap;
                                    RoundFragment.this.googleMap.setMyLocationEnabled(false);
                                    RoundFragment.this.googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                                    List<PolylineOptions> polylines = new ArrayList<>();
                                    List<LatLng> points = new ArrayList<LatLng>();
                                    for (Round r : finalRealmListRound) {
                                        //Lokasyonları belirle
                                        for (int i = 0; i < r.getLocationses().size(); i++) {
                                            builder.include(new LatLng(r.getLocationses().get(i).getLatitude(), r.getLocationses().get(i).getLongitude()));
                                            if (points.size() > 1) {
                                                android.location.Location A = onLocationDefined("Point A", r.getLocationses().get(i));
                                                android.location.Location B = onLocationDefined("Point B", r.getLocationses().get(i - 1));

                                                if (onDistanceCalculation(A, B) >= 0.05) {
                                                    polylines.add(new PolylineOptions().addAll(points).width(10).color(R.color.colorHint).geodesic(true).addAll(points).width(10).color(R.color.colorHint).geodesic(true));
                                                    RoundFragment.this.googleMap.addPolyline(polylines.get(polylines.size() - 1));
                                                    points.clear();
                                                }
                                            }
                                            points.add(new LatLng(r.getLocationses().get(i).getLatitude(), r.getLocationses().get(i).getLongitude()));
                                        }

                                        //Lokasyonları çiz
                                        polylines.add(new PolylineOptions().addAll(points).width(10).color(R.color.colorHint).geodesic(true));
                                        RoundFragment.this.googleMap.addPolyline(polylines.get(polylines.size() - 1));
                                        points.clear();
                                    }
                                    //First
                                    LatLng first = polylines.get(0).getPoints().get(0);
                                    //Last
                                    LatLng last = polylines.get(polylines.size() - 1).getPoints().get(polylines.get(polylines.size() - 1).getPoints().size() - 1);

                                    //Add Market
                                    RoundFragment.this.googleMap.addMarker(new MarkerOptions().anchor(0.5f, 0.55f).snippet("0 km.").position(first).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_002_man)).title("STARTED"));
                                    RoundFragment.this.googleMap.addMarker(new MarkerOptions().anchor(0.5f, 0.55f).snippet(String.format("%.2f", args.getDouble("runKilometer")) + " km.").position(last).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_001_victory)).title("FINISHED"));

                                    //Camera Position

                                    LatLngBounds bounds = builder.build();
                                    final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                    googleMap.animateCamera(cameraUpdate);

                                    progressDialog.dismiss();
                                }
                            }
                        }
                    });
                }
            }
        }

    }

    private void readBundle() {
        Bundle args = getArguments();
        if (args != null) {

            //Run Name
            runName = args.getString("runName");
            //Kilometers
            String sumKilometers = String.format("%.2f", args.getDouble("runKilometer"));
            //Date
            String createdAt = DateFormat.format("dd/MM/yyyy", new Date(args.getLong("runDate"))).toString();
            //Pace
            int paceMin = Integer.valueOf(new SimpleDateFormat("mm").format(args.getDouble("runAvrPace")));
            int paceSecond = Integer.valueOf(new SimpleDateFormat("ss").format(args.getDouble("runAvrPace")));
            String avrPace = paceMin + "'" + paceSecond + "''";
            //SumTime
            String runTime = (String) DateFormat.format("mm:ss", args.getLong("runTime"));

            textViewTag.setText(runName.toUpperCase());
            textViewSumKM.setText(sumKilometers + " KM.");
            textViewDate.setText(createdAt);
            textViewAvrPace.setText(avrPace);
            textViewSumTime.setText(runTime);
        } else {
            Log.d("BUNDLE", "RoundFragment info isEmpty : True");
        }

        showRecord();
    }

    private double onDistanceCalculation(android.location.Location _locationA, android.location.Location _locationB) {

        final double distance = _locationA.distanceTo(_locationB) / 1000;

        Log.i("LOCATION_DISTANCE", String.valueOf(_locationA));
        Log.i("LOCATION_DISTANCE", String.valueOf(_locationB));
        Log.i("LOCATION_DISTANCE", "Distance :" + String.valueOf(distance));

        return distance;

    }

    private android.location.Location onLocationDefined(String _name, Location _location) {
        android.location.Location location = new android.location.Location(_name);

        location.setLatitude(_location.getLatitude());
        location.setLongitude(_location.getLongitude());
        location.setAltitude(_location.getAltitude());
        location.setSpeed(_location.getSpeed());
        location.setAccuracy(_location.getAccuracy());
        location.setBearing(_location.getBearing());

        return location;
    }

    private void showRecord() {
        if (getArguments().getDouble("runKilometer") >= 1) {
            textViewNoRecord.setVisibility(View.GONE);
            linearLayoutList.setVisibility(View.VISIBLE);
        } else {
            textViewNoRecord.setVisibility(View.VISIBLE);
            linearLayoutList.setVisibility(View.GONE);
        }
    }

    public Animator newPageAnimator(View content) {
        final int x = (content.getLeft() + content.getRight()) / 2;
        final int y = (content.getTop() + content.getBottom()) / 2;

        final float finalRounds = (float) Math.max(content.getWidth(), content.getHeight());

        content.setVisibility(View.VISIBLE);

        Animator animator = ViewAnimationUtils.createCircularReveal(content, x, y, 0, finalRounds);
        animator.setDuration(1000);
        return animator;
    }
}