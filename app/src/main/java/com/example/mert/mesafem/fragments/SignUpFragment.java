package com.example.mert.mesafem.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.mesafem.bitmaps.BitmapModifyOrientation;
import com.example.mert.mesafem.R;
import com.example.mert.mesafem.dialogs.SelectionPhotoDialog;
import com.example.mert.mesafem.interfaces.OnChangePhotoListener;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpFragment extends BaseFragment implements OnChangePhotoListener {

    private SelectionPhotoDialog selectionPhotoDialog;

    private Button buttonNext;

    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextEmail;
    private EditText editTextHeight;
    private EditText editTextWeight;

    private TextView textViewFirstName;
    private TextView textViewLastName;
    private TextView textViewEmail;
    private TextView textViewHeight;
    private TextView textViewWeight;

    private ImageView imageButtonWarningFirstName;
    private ImageView imageButtonWarningLastName;
    private ImageView imageButtonWarningEmail;
    private ImageView imageButtonWarningHeight;
    private ImageView imageButtonWarningWeight;
    private CircleImageView imageViewProfilImage;

    private String[] editTextsText;
    private String[] textViews;
    private ImageView[] warnings;

    private String profilImagePath = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sign, container, false);

        setPageName("SignUp");
        setHeaderTextActionBar(getResources().getString(R.string.app_name));
        setButtonVisibilityActionBar(false, false);

        selectionPhotoDialog = new SelectionPhotoDialog(baseActivity);
        baseActivity.setChangePhotoListener(SignUpFragment.this);

        buttonNext = (Button) view.findViewById(R.id.button_save);
        editTextFirstName = (EditText) view.findViewById(R.id.editText_first_name_u);
        editTextLastName = (EditText) view.findViewById(R.id.editText_last_name_u);
        editTextEmail = (EditText) view.findViewById(R.id.editText_email_u);
        editTextHeight = (EditText) view.findViewById(R.id.editText_height_u);
        editTextWeight = (EditText) view.findViewById(R.id.editText_weight_u);

        imageViewProfilImage = (CircleImageView) view.findViewById(R.id.imageView_profil_image);

        Log.d("PROFIL_PHOTO", "START signUp");
        profilImagePath = setProfilImage();

        textViewFirstName = (TextView) view.findViewById(R.id.textView_first_name);
        textViewLastName = (TextView) view.findViewById(R.id.textView_last_name);
        textViewEmail = (TextView) view.findViewById(R.id.textView_email);
        textViewHeight = (TextView) view.findViewById(R.id.textView_height);
        textViewWeight = (TextView) view.findViewById(R.id.textView_weight);

        imageButtonWarningFirstName = (ImageView) view.findViewById(R.id.imageView_first_name);
        imageButtonWarningLastName = (ImageView) view.findViewById(R.id.imageView_last_name);
        imageButtonWarningEmail = (ImageView) view.findViewById(R.id.imageView_email);
        imageButtonWarningHeight = (ImageView) view.findViewById(R.id.imageView_height);
        imageButtonWarningWeight = (ImageView) view.findViewById(R.id.imageView_weight);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ControlContent()) {
                    sharedPreference.setProfil(
                            editTextsText[0],
                            editTextsText[1],
                            editTextsText[2],
                            Float.parseFloat(editTextsText[3]),
                            Float.parseFloat(editTextsText[4]),
                            profilImagePath);

                    replaceFragment(new MainStartFragment());
                }
            }
        });

        imageViewProfilImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("IMAGE_CLICK", "tıklandı.");
                selectionPhotoDialog.create();
            }
        });

        return view;
    }

    private boolean ControlContent() {
        warnings = new ImageView[]{
                imageButtonWarningFirstName,
                imageButtonWarningLastName,
                imageButtonWarningEmail,
                imageButtonWarningHeight,
                imageButtonWarningWeight
        };

        for (ImageView item : warnings) {
            Log.d("VISIBILITY", item.toString() + " => GONE");
            item.setVisibility(View.GONE);
        }

        editTextsText = new String[]{
                editTextFirstName.getText().toString(),
                editTextLastName.getText().toString(),
                editTextEmail.getText().toString(),
                editTextHeight.getText().toString(),
                editTextWeight.getText().toString()
        };

        textViews = new String[]{
                textViewFirstName.getText().toString(),
                textViewLastName.getText().toString(),
                textViewEmail.getText().toString(),
                textViewHeight.getText().toString(),
                textViewWeight.getText().toString()
        };

        for (int item = 0; item < editTextsText.length; item++) {
            Log.d("ITEM", String.valueOf(item));
            if (editTextsText[item].isEmpty()) {
                Log.d("VISIBILITY", warnings[item] + " => VISIBLE");
                warnings[item].setVisibility(View.VISIBLE);
                Toast.makeText(baseActivity, "Please enter " + textViews[item].toLowerCase() + ".", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if ((editTextsText[2].substring(0, 1) == "@")) {
            Log.d("EMAIL_CONTROL", ".substring(0, 1) == @");
            warnings[2].setVisibility(View.VISIBLE);
            Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format.", Toast.LENGTH_LONG).show();
            return false;
        } else if ((editTextsText[2].indexOf(" ") != -1)) {
            Log.d("EMAIL_CONTROL", ".indexOf(' ') != -1");
            Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format or leave empty", Toast.LENGTH_LONG).show();
            return false;
        } else if ((editTextsText[2].indexOf("@") == -1)) {
            Log.d("EMAIL_CONTROL", ".indexOf(@) == -1");
            warnings[2].setVisibility(View.VISIBLE);
            Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format.", Toast.LENGTH_LONG).show();
            return false;
        } else if ((editTextsText[2].indexOf("@.com") != -1)) {
            Log.d("EMAIL_CONTROL", ".indexOf(@) == -1");
            warnings[2].setVisibility(View.VISIBLE);
            Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format.", Toast.LENGTH_LONG).show();
            return false;
        } else if ((editTextsText[2].indexOf(".com") == -1)) {
            Log.d("EMAIL_CONTROL", ".indexOf(.com) == -1");
            warnings[2].setVisibility(View.VISIBLE);
            Toast.makeText(baseActivity, "Please enter your email address '...@....com' Enter the format.", Toast.LENGTH_LONG).show();
            return false;
        }

        if (editTextsText[3].indexOf('.') != -1) {
            if (Integer.parseInt(editTextsText[3].substring(0, editTextsText[3].indexOf("."))) < 100) {
                Log.d("HEIGHT_CONTROL", "Invalid height.");
                warnings[3].setVisibility(View.VISIBLE);
                Toast.makeText(baseActivity, "Your height can't be less than 100 meters.", Toast.LENGTH_LONG).show();
                return false;
            } else {
                if (Integer.parseInt(editTextsText[3]) < 100) {
                    Log.d("HEIGHT_CONTROL", "Invalid height.");
                    warnings[3].setVisibility(View.VISIBLE);
                    Toast.makeText(baseActivity, "Your height can't be less than 100 meters.", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }

        if (editTextsText[4].indexOf('.') != -1) {
            if (Integer.parseInt(editTextsText[4].substring(0, editTextsText[4].indexOf("."))) < 40) {
                Log.d("HEIGHT_CONTROL", "Invalid height.");
                warnings[4].setVisibility(View.VISIBLE);
                Toast.makeText(baseActivity, "Your height can't be less than 40 kilograms.", Toast.LENGTH_LONG).show();
                return false;
            } else {
                if (Integer.parseInt(editTextsText[4]) < 40) {
                    Log.d("HEIGHT_CONTROL", "Invalid height.");
                    warnings[4].setVisibility(View.VISIBLE);
                    Toast.makeText(baseActivity, "Your height can't be less than 40 kilograms.", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }

        if (profilImagePath != "") {
            sharedPreference.setProfilPhoto(profilImagePath);
        }

        return true;
    }

    private String setProfilImage() {
        String path = sharedPreference.getProfilPhoto();
        Log.i("PROFIL_PHOTO", "GET PHOTO FILE : " + path);
        if (path != "") {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            try {
                imageViewProfilImage.setImageBitmap(new BitmapModifyOrientation().modifyOrientation(bitmap, path));
            } catch (IOException e) {
                Toast.makeText(baseActivity, "Measurement error in the photo!", Toast.LENGTH_SHORT).show();
                imageViewProfilImage.setImageBitmap(bitmap);
                e.printStackTrace();
            }
        }
        return path;
    }

    @Override
    public void ChangePhoto(String path) {
        Log.i("PHOTO","getChangePhoto");
        profilImagePath = path;
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        try {
            imageViewProfilImage.setImageBitmap(new BitmapModifyOrientation().modifyOrientation(bitmap, path));
        } catch (IOException e) {
            Toast.makeText(baseActivity, "Measurement error in the photo!", Toast.LENGTH_SHORT).show();
            imageViewProfilImage.setImageBitmap(bitmap);
            e.printStackTrace();
        }
    }
}
