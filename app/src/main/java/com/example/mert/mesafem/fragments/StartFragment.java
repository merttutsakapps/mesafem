package com.example.mert.mesafem.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.location.OnLocationChangeListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class StartFragment extends BaseFragment implements OnLocationChangeListener {
    private MapView mMapView;
    private GoogleMap googleMap;
    private ImageButton buttonPlay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_start, container, false);
        buttonPlay = (ImageButton) view.findViewById(R.id.button_filigram_play);
        progressDialog.onCreateDialog(null);

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (googleMap != null) {
                        StartFragment.this.googleMap = googleMap;
                        StartFragment.this.googleMap.setMyLocationEnabled(true);
                        StartFragment.this.googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        StartFragment.this.googleMap.getUiSettings().setAllGesturesEnabled(false);
                        StartFragment.this.googleMap.getUiSettings().setZoomControlsEnabled(false);
                        StartFragment.this.googleMap.getUiSettings().setCompassEnabled(false);
                        baseActivity.locationBroadcastReceiver.setOnLocationChangeListener(StartFragment.this);
                        StartFragment.this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                        progressDialog.dismiss();
                    }
                }
            }
        });

        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new PlayFragment());
            }
        });
        return view;
    }

    @Override
    public void onLocationChange(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        //googleMap.clear();
        //googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.myLocation)));
        //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)      // Sets the center of the map to Mountain View
                .zoom(15)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 0 degrees
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
