package com.example.mert.mesafem.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Run extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private RealmList<Round> rounds;
    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        setCreatedAt(new Date());
    }

    public RealmList<Round> getRounds() {
        return rounds;
    }

    public void setRounds(RealmList<Round> rounds) {
        this.rounds = rounds;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Run{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rounds=" + rounds +
                ", createdAt=" + createdAt +
                '}';
    }
}
