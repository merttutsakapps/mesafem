package com.example.mert.mesafem.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by mert on 26/07/2017.
 */

public class GoogleMapFragment extends SupportMapFragment {
    private static final String SUPPORT_MAP_BUNDLE_KEY = "MapOptions";
    private OnGoogleMapFragmentListener listener;

    public static GoogleMapFragment newInstance() {
        return new GoogleMapFragment();
    }

    public static GoogleMapFragment newInstance(GoogleMapOptions options) {
        GoogleMapFragment fragment = new GoogleMapFragment();

        if (options != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(SUPPORT_MAP_BUNDLE_KEY, options);

            fragment.setArguments(arguments);
        }

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = super.onCreateView(layoutInflater, viewGroup, bundle);

        if (listener != null) {
            getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    listener.onMapReady(googleMap);
                }
            });
        }

        return view;
    }

    public void setOnGoogleMapFragmentListener(OnGoogleMapFragmentListener listener) {
        this.listener = listener;
    }

    public interface OnGoogleMapFragmentListener {
        void onMapReady(GoogleMap map);
    }
}
