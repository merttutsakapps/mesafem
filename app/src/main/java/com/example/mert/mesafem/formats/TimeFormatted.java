package com.example.mert.mesafem.formats;

public class TimeFormatted {

    public String convertTimeToHour(long _time) {
        return getHour(_time) + ":" + getMinute(_time) + ":" + getSecond(_time);
    }

    public String convertTimeToMinute(long _time) {
        return getMinute(_time) + ":" + getSecond(_time);
    }

    public String convertTimeToSecond(long _time) {
        return getSecond(_time);
    }

    public String convertPace(long _time) {
        return getMinute(Math.abs(_time)) + "'" + getSecond(Math.abs(_time)) + "''";
    }

    public String getHour(long _time) {
        int hour = (int) Math.floor(_time / 36000000);
        if (hour >= 10) {
            return String.valueOf(hour);
        } else if (_time >= 1) {
            return "0" + String.valueOf(hour);
        } else {
            return "0";
        }
    }

    public String getMinute(long _time) {
        int minute = (int) Math.floor((_time / 60000) % 60);
        if (minute >= 10) {
            return String.valueOf(minute);
        } else if (minute >= 1) {
            return "0" + String.valueOf(minute);
        } else {
            return "0";
        }
    }

    public String getSecond(long _time) {
        int second = (int) Math.floor((_time / 1000) % 60);
        if (second >= 10) {
            return String.valueOf(second);
        } else if (second >= 1) {
            return "0" + String.valueOf(second);
        } else {
            return "0";
        }
    }
}