package com.example.mert.mesafem.fragments;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.adapters.ExpandableListAdapter;
import com.example.mert.mesafem.dialogs.RunDeleteDialog;
import com.example.mert.mesafem.interfaces.OnRunDeleteChoiceListener;
import com.example.mert.mesafem.model.Header;
import com.example.mert.mesafem.model.Run;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class HistoryFragment extends BaseFragment implements OnChildClickListener, AdapterView.OnItemLongClickListener, OnRunDeleteChoiceListener {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private TextView textViewNoRecord;

    private List<Header> headerList;

    private boolean click = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_history, container, false);

        textViewNoRecord = (TextView) view.findViewById(R.id.textView_no_record);
        expListView = (ExpandableListView) view.findViewById(R.id.expandableListView_run);

        headerList = prepareListData();

        listAdapter = new ExpandableListAdapter(baseActivity, headerList);
        expListView.setAdapter(listAdapter);
        expListView.setOnItemLongClickListener(this);
        expListView.setOnChildClickListener(this);

        //for (int index = 0; index < listAdapter.getGroupCount(); index++) {expListView.collapseGroup(index);}

        return view;
    }

    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (click) {
            List<Header> headers = new ArrayList<>();
            headers.clear();

            headers = listAdapter.getHeaders();
            if (parent != null) {
                if (v != null) {
                    if (!headers.isEmpty()) {
                        Bundle args = new Bundle();

                        args.putString("runName", headers.get(groupPosition).getRuns().get(childPosition).getName());
                        args.putDouble("runKilometer", headers.get(groupPosition).getRuns().get(childPosition).getRounds().sum("kilometers").doubleValue());
                        args.putLong("runDate", headers.get(groupPosition).getRuns().get(childPosition).getCreatedAt().getTime());
                        args.putDouble("runAvrPace", headers.get(groupPosition).getRuns().get(childPosition).getRounds().average("pace"));
                        args.putLong("runTime", headers.get(groupPosition).getRuns().get(childPosition).getRounds().sort("id", Sort.ASCENDING).last().getTime());

                        RoundFragment fragment = RoundFragment.newInstance(args);

                        if (!listAdapter.isEmpty()) {
                            expListView.expandGroup(groupPosition);
                        }

                        replaceFragment(fragment);
                    }
                }
            }
        }
        return false;
    }

    private List<Header> prepareListData() {
        List<Header> headers = new ArrayList<>();
        headers.clear();

        RealmResults<Run> runRealmResults = Realm.getDefaultInstance().where(Run.class).findAllSorted("createdAt", Sort.DESCENDING);
        runRealmResults.load();

        if (!runRealmResults.isEmpty()) {
            textViewNoRecord.setVisibility(View.GONE);
            expListView.setVisibility(View.VISIBLE);
            for (int i = 0; i < runRealmResults.size(); i++) {
                Run run = runRealmResults.get(i);

                String year = DateFormat.format("yyyy", new Date(run.getCreatedAt().getTime())).toString();
                String month = DateFormat.format("MMMM", new Date(run.getCreatedAt().getTime())).toString();
                String headerName = month + " " + year;

                Log.d("MODELLIST_PREPARE", run.toString());
                Log.d("MODELLIST_PREPARE", headerName);

                if (headers.size() == 0) {
                    Header header = new Header();
                    header.setHeaderName(headerName);
                    header.addRun(run);
                    headers.add(header);
                } else {
                    int index = headers.size() - 1;
                    if (headers.get(index).getHeaderName().equals(headerName)) {
                        headers.get(index).addRun(run);
                    } else {
                        Header header = new Header();
                        header.setHeaderName(headerName);
                        header.addRun(run);
                        headers.add(header);
                    }
                }
            }
            Log.i("EXPANDABLE_LIST", "Size :" + String.valueOf(runRealmResults.size()) + " Count : " + headers.size());
        } else {
            textViewNoRecord.setVisibility(View.VISIBLE);
            expListView.setVisibility(View.GONE);
        }
        return headers;
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        click = false;
        if (!click) {
            long packedPosition = expListView.getExpandableListPosition(position);

            int itemType = ExpandableListView.getPackedPositionType(packedPosition);
            int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
            int childPosition = ExpandableListView.getPackedPositionChild(packedPosition);

            String runName = "";
            if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {

            } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                List<Header> headers = new ArrayList<>();
                headers.clear();
                headers = listAdapter.getHeaders();
                if (!headers.isEmpty()) {
                    runName = headers.get(groupPosition).getRuns().get(childPosition).getName();
                    RunDeleteDialog runDeleteDialog = new RunDeleteDialog(baseActivity, runName, position, id);
                    runDeleteDialog.setOnRunDeleteChoiceListener(HistoryFragment.this);
                    runDeleteDialog.create();
                }
            }
        }
        return false;
    }

    @Override
    public void onDeleteChoice(Boolean choice, String name, int position, long id) {
        if (choice) {
            long packedPosition = expListView.getExpandableListPosition(position);
            int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);

            Realm.getDefaultInstance().beginTransaction();
            RealmResults<Run> rows = Realm.getDefaultInstance().where(Run.class).equalTo("name", name).findAll();
            rows.deleteAllFromRealm();
            Realm.getDefaultInstance().commitTransaction();

            headerList = prepareListData();
            listAdapter = new ExpandableListAdapter(baseActivity, headerList);
            expListView.setAdapter(listAdapter);

            if (!listAdapter.isEmpty()) {
                expListView.expandGroup(groupPosition);
            }
        }
        click = true;
    }
}


