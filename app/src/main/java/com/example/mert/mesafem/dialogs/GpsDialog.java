package com.example.mert.mesafem.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mert.mesafem.R;
import com.example.mert.mesafem.activities.BaseActivity;
import com.example.mert.mesafem.fragments.BaseFragment;
import com.example.mert.mesafem.interfaces.OnRespondListener;

public class GpsDialog extends Dialog {

    private BaseActivity baseActivity;
    private OnRespondListener onRespondListener;
    private Button buttonOkey;
    private Button buttonClose;

    private String choice;
    private BaseFragment fragment;

    public GpsDialog(@NonNull Context context, String choice, BaseFragment fragment) {
        super(context);
        this.baseActivity = (BaseActivity) context;
        this.choice = choice;
        this.fragment = fragment;
    }

    @Override
    public void create() {
        super.create();
        this.setContentView(R.layout.dialog_gps);
        this.setCancelable(false);

        buttonOkey = (Button) this.findViewById(R.id.button_gps_open);
        buttonClose = (Button) this.findViewById(R.id.button_app_close);

        buttonOkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!baseActivity.getInfoGPS()) {
                    Toast.makeText(baseActivity, "Please try again after checking!", Toast.LENGTH_SHORT).show();
                } else {
                    onRespondListener.onChoice(choice, fragment);
                    dismiss();
                }
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.finish();
            }
        });

        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void show() {
        super.show();
    }

    public void setOnRespondListener(OnRespondListener onRespondListener) {
        Log.d(onRespondListener.getClass().getSimpleName(), onRespondListener != null ? "onRespondListener null degil" : "-----null-------");
        this.onRespondListener = onRespondListener;
    }
}
